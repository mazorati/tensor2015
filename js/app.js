/**
 * Created by Aleksandr Mavrichev on 28.10.15.
 */

if (!Array.prototype.last){
    Array.prototype.last = function(){
        return this[this.length - 1];
    };

    //Выбравниваем массивы
    Array.prototype.setNewRange = function(n){
        var result = [];
        for (var i = 0; i < n; i++)
            result[i] = i < this.length ? this[i] : 0;
        return result;
    };
}

//Класс LongCalc

function LongCalc() {

    this.a = [];
    this.b = [];
    this.result = []; // Массив для результата
    this.minus = false;
    this.allow = true;

    this.setValues = function(string1,string2){
        this.result = [];
        this.minus = false;
        this.allow = true;
        this.a = prepareArrayFromString(string1);
        this.b = prepareArrayFromString(string2);
        this.prepareArrays();
        console.log(this.a,this.b,this.minus);

        return this;
    };

    //Сравнивает числа.
    // -1 - второе больше первого
    // 1 - первое больше второго
    // 0 - равны

    this.compare = function(){
        if (this.a.last() < this.b.last()) return -1;
        if (this.a.last() > this.b.last()) return 1;
        for (var i = this.a.length - 1; i <= 0; i--)
        {
            if (this.a[i] < this.b[i]) return -1;
            if (this.a[i] >= this.b[i]) return 1;
        }

        return 0;
    };

    //Функция для смены местами массивов (большего и меньшего)
    this.prepareArrays = function(){
        var temp = [];

        if(this.a.length == 0 || this.b.length == 0) {
            alert("Число(а) определены не верно. Вычисление не может быть выполнено.");
            this.allow = false;
            return;
        }

        if (this.a.length < this.b.length)
        {
            temp = this.b;
            this.b = this.a.setNewRange(this.b.length);
            this.a = temp;
            this.minus = true;
        }
        else if (this.a.length > this.b.length)
        {
            this.b = this.b.setNewRange(this.a.length);
        }
        else if (this.compare() == -1) {
                temp = this.a;
                this.a = this.b;
                this.b = temp;
                this.minus = true;
        }
    };

    //Сложение в столбик

    this.sum = function(){

        if(!this.allow) return self;

        var s = 0,
            c = [];

        for (var i = 0; i < this.a.length; i++)
        {
            c[i] = this.a[i] + this.b[i] + s;
            if (c[i] < 10) s = 0; else {
                s = 1;
                c[i] = c[i] - 10;
                if (i + 1 == this.a.length) c[i + 1] = s;
            }

        }

        this.result = c.reverse();

        return this;
    };

    //Вычитание

    this.sub = function(){

        if(!this.allow) return self;

        var c = [],
            cl = this.a.length,
            s = 0;

        for (var i = 0; i < cl; i++)
        {
            c[i] = this.a[i] - this.b[i] - s;

            if (c[i] >= 0) s = 0;
                else {
                s = 1;
                c[i] = c[i] + 10;
            }
        }

        //Удаляет лишние нули
        if(c.length > 1){
            for (i = c.length-1; i > 0; i--){
                if(c[i] == 0) {
                    c.pop();
                } else break;
            }
        }

        this.result = c.reverse();

        return this;

    };

    //Функция умножения

    this.mult = function(){

        if(!this.allow) return self;

        this.minus = false;

        console.log(this.a,this.b);

        var k = [];
        var kl = this.a.length + this.b.length;

        //Подготовка массивов

        for (var i = 0; i < this.a.length; i++) k[i] = this.a[i];

        for (i = this.a.length; i < kl; i++) k[i] = 0;

        var c = [];
        for (i = 0; i < kl; i++) c[i] = 0;

        //Умножение

        var tt = 0;

        for (i = 0; i < this.b.length; i++)
        {
            for (var j = 0; j < this.a.length; j++)
            {
                c[j+i] += this.b[i] * this.a[j];

                tt = c[j+i]/10|0;

                if(tt > 0){
                    for (var m = j+i+1; m < c.length; m++){
                        c[m] = tt + c[m];
                        tt = c[m]/10|0;
                        c[m] -= tt*10;
                        if(tt == 0) break;
                    }
                }

                c[j+i] -= (c[j+i]/10|0) * 10;
            }
        }

        console.log(c);

        //Удаляет лишние нули
        if(c.length > 1){
            for (i = c.length-1; i > 0; i--){
                if(c[i] == 0) {
                    c.pop();
                } else break;
            }
        }

        console.log(c);

        this.result = c.reverse();

        return this;
    };

    this.resultString = function(){

        return this.result.length > 0 ? ((this.minus)?'-':'') + this.result.join("") : '';
    };

    //Подготовит массив на основе чисел в веденной строке (обратный! - нужно для дальнейших вычислений)
    function prepareArrayFromString(string){
        var result = [];
        var flag = false;

        string = string.replace(/[^\/\d]/g,''); //Убрать все лишние символы

        if(string.length > 64 || string.length == 0){
            alert('Число не определено или его длина превышает 64 символа.');
            return result;
        } //Проверка на длину

        for (var i = 0; i < string.length; i++) {

            if(!flag) flag = (parseInt(string[i]) > 0); //Убрать все 0 вначале

            if(flag) result[i] = parseInt(string[i]);

        }

        if(result.length == 0) result = [0];

        result.reverse(); //Поворот нужен для дальнейших расчетов
        return result;
    }

}


jQuery(function($){

    var calc = new LongCalc(),
        $input1 = $("#input1"),
        $input2 = $("#input2"),
        $input3 = $("#input3"),
        $operation = $("#operation");

    function handleChanges(e){

        var val1 = $input1.val(),
            val2 = $input2.val();

        if(val1.length > 0 && val2.length > 0){
            calc.setValues(val1,val2);

            switch(parseInt($operation.val())){
                case 1:
                    calc.sub();
                    break;
                case 2:
                    calc.mult();
                    break;
                default:
                    alert('Такого действия не существует.');
            }

            $input3.val(calc.resultString());
        }
    }

    $input1.on('input',handleChanges);
    $input2.on('input',handleChanges);
    $operation.change(handleChanges);

    $operation.change();


});

//console.log(calc.mult().resultString());
//
//console.log(calc.setValues("4567","123").mult().resultString());
//console.log(calc.setValues("130","11").mult().resultString());
//console.log(calc.setValues("1000","120").mult().resultString());
//console.log(calc.setValues("0","12").mult().resultString());
//console.log(calc.setValues("800","1").mult().resultString());